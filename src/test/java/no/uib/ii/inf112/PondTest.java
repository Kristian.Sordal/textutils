package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import no.uib.ii.inf112.pond.Pond;
import no.uib.ii.inf112.pond.PondObject;

public class PondTest {

    Pond myPond = new Pond(1280, 720);

    @Test
    void numberOfDucklingsTest() {
        assertEquals(1, myPond.objs.size());

        int s = 1;
        for (int i = 1; i <= 100; i++) {
            myPond.step();
            if (i % 25 == 0) {
                for (PondObject o : myPond.objs) {
                    if (o.getSize() >= 50) {
                        s++;
                    }
                }
                assertEquals(s, myPond.objs.size());
            }
        }
    }

    @Test
    void testGrowth() {
        List<PondObject> prev = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < prev.size(); j++) {
                assertEquals(prev.get(j).getSize() + 1, myPond.objs.get(j).getSize());
            }
            prev.clear();
            myPond.step();

            for (PondObject o : myPond.objs) {
                prev.add(o);
            }
        }
    }

    @Test
    void correctPositionTest() {

        // Initial position
        assertEquals(0, myPond.objs.get(0).getX());

        for (int i = 1; i <= 100; i++) {
            myPond.step();

            assertEquals(i * (-1), myPond.objs.get(0).getX());
        }
    }
}
