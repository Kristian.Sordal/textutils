package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	// TextAligner aligner = new TextAligner() {

	MyTextAligner aligner = new MyTextAligner();
	// public String center(String text, int width) {
	// int extra = (width - text.length()) / 2;
	// return " ".repeat(extra) + text + " ".repeat(extra);
	// }

	// public String flushRight(String text, int width) {
	// // TODO Auto-generated method stub
	// return null;
	// }

	// public String flushLeft(String text, int width) {
	// // TODO Auto-generated method stub
	// return null;
	// }

	// public String justify(String text, int width) {
	// // TODO Auto-generated method stub
	// return null;
	// }};

	@Test
	void test() {
		assertTrue(true);
		// fail("Not yet implemented");
	}

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
		assertEquals(" foo  ", aligner.center("foo", 6));
	}

	@Test
	void testRight() {
		assertEquals("    A", aligner.flushRight("A", 5));
		assertEquals("    Heisann", aligner.flushRight("Heisann", 11));
	}

	@Test
	void testLeft() {
		assertEquals("A    ", aligner.flushLeft("A", 5));
		assertEquals("A B C D E    ", aligner.flushLeft("A B C D E", 13));

	}

	// @Test
	// void testJustify() {
	// assertEquals("A B C", aligner.justify("A B C", 7));
	// assertEquals("A B C", aligner.justify("A B C", 8));
	// // assertEquals("A B C", aligner.justify("A B C", 6));
	// }
}
