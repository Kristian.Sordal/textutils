package no.uib.ii.inf112;

import java.util.Scanner;

public class FizzBuzz {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a number: ");
        int num1 = in.nextInt();
        System.out.println("Enter another number: ");
        int num2 = in.nextInt();

        System.out.println("What do you want to print when the number is divisble by " + num1 + "? ");
        String str1 = in.next();

        System.out.println("What do you want to print when the number is divisble by " + num2 + "? ");
        String str2 = in.next();

        System.out.println("How long do you want to " + str1 + str2 + " for? ");
        int l = in.nextInt();

        for (int i = 1; i <= l; i++) {
            if (i % num1 == 0) {
                if (i % num2 == 0) {
                    System.out.println(str1 + str2);
                }
                System.out.println(str1);
            } else if (i % num2 == 0) {
                System.out.println(str2);
            } else {
                System.out.println(i);
            }
        }

        in.close();
    }
}
