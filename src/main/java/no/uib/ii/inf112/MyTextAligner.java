package no.uib.ii.inf112;

public class MyTextAligner implements TextAligner {
    @Override
    public String center(String text, int width) {
        if (width < text.length()) {
            throw new IllegalArgumentException("Width smaller than text-length.");
        }

        int sCount = width - text.length();
        String s = " ".repeat(sCount / 2);
        return sCount % 2 == 0 ? s + text + s : s + text + s + " ";
    }

    @Override
    public String flushRight(String text, int width) {
        if (width < text.length()) {
            throw new IllegalArgumentException("Width smaller than text-length.");
        }
        return " ".repeat(width - text.length()) + text;
    }

    @Override
    public String flushLeft(String text, int width) {
        if (width < text.length()) {
            throw new IllegalArgumentException("Width smaller than text-length.");
        }
        return text + " ".repeat(width - text.length());
    }

    @Override
    public String justify(String text, int width) {
        if (width < text.length()) {
            throw new IllegalArgumentException("Width smaller than text-length.");
        }
        int spaces = width - text.length();

        String newText = "";

        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == ' ' && spaces >= 0) {
                newText += text.charAt(i) + " ";
                spaces -= 2;
            } else {
                newText += text.charAt(i);
            }
        }
        return newText;
    }

}
